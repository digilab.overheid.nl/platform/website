/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './content/**/*.md',
    './layouts/**/*.html',
    './assets/js/**/*.ts',
  ],
  theme: {
    extend: {
      typography: ({ theme }) => ({
        DEFAULT: {
          css: {
            '--tw-prose-body': theme('colors.gray.800'),
            '--tw-prose-headings': theme('colors.gray.800'),
            '--tw-prose-lead': theme('colors.gray.800'),
            '--tw-prose-links': theme('colors.blue.600'),
            '--tw-prose-code': theme('colors.slate.800'),
            '--tw-prose-pre-code': theme('colors.slate.100'),
            '--tw-prose-pre-bg': theme('colors.slate.800'),
            '--tw-prose-th-borders': theme('colors.slate.300'),
            '--tw-prose-td-borders': theme('colors.slate.200'),
            'a:hover': {
              'text-decoration': 'none',
            },
            h1: {
              'font-weight': '700', // Less bold than default
              'font-size': '2rem', // Slightly smaller than the h1 that is used for the title
            },
            code: {
              padding: '0.25rem 0.375rem',
              'background-color': theme('colors.slate.200'),
              'border-radius': '0.125rem',
            },
            'code::before': { // Note: instead of '&::before' in the block above, since that will result in duplicate selectors
              content: '""',
            },
            'code::after': {
              content: '""',
            },
            'a code': {
              color: theme('colors.blue.700'),
            },
            th: {
              'text-align': 'left',
              padding: '0.375rem 1rem',
              'background-color': theme('colors.slate.100'),
            },
            td: {
              padding: '0.375rem 1rem',
            }
          },
        },
      }),
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
}
