FROM golang:1.23-alpine3.20 as build

# Install Hugo. Note: this installs the extended version
# Install Git, which is required for Hugo modules
# Note: npm installation is currently required to due this bug: https://github.com/gohugoio/hugo/issues/11914
RUN apk add --update hugo git npm

# Install Node.js. Note: the nodejs package does not include corepack, so we use nodejs-current
RUN apk add --no-cache nodejs-current

RUN corepack enable && corepack prepare pnpm@latest --activate

# Copy the repo content to /site
WORKDIR /site

COPY package.json pnpm-lock.yaml ./

# Run pnpm install to build the dependencies
RUN pnpm install

COPY . .

# Run Hugo to build the static files
RUN hugo --gc

# Serve the files using nginx (preferred over serving them with the Hugo server)
FROM nginx:1.27.1

COPY --from=build /site/public /usr/share/nginx/html

COPY ./nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
