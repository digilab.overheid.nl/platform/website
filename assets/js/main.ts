import tippy from 'tippy.js';

// Define types for importing calendar items. Note: other attributes and subtypes are omitted for now, since these are not used in the template
interface Activity {
  entity: Entity;
}

interface Entity {
  title: string;
  // excerpt: string;
  // canEdit: boolean;
  // subtype: string;
  url: string;
  // votes: any;
  // hasVoted: any;
  // isBookmarked: boolean;
  // isPinned: boolean;
  // inGroup: boolean;
  // canBookmark: boolean;
  // tagCategories: any[];
  rsvp: boolean;
  // isFeatured: boolean;
  // featured: Featured;
  startDate: string;
  endDate: string;
  // location: string;
  // locationLink: string;
  // timePublished: string;
  // statusPublished: string;
  // commentCount: number;
  // canComment: boolean;
  // owner: Owner;
  // isAttending: any;
  attendees: Attendees;
  // group: Group;
  // __typename: string;
}

interface Attendees {
  totalAccept: number;
  // edges: any[];
  // __typename: string;
}

// Sidebar toggle. Note: the menu toggle is implemented using pure CSS with a hidden checkbox
document.getElementById('sidebar-toggle')?.addEventListener('click', () => {
  document.body.classList.toggle('show-sidebar');
});

// Close the menu and sidebar on backdrop click
const menuInputEl = document.getElementById('menu-input') as HTMLInputElement;
for (const el of document.querySelectorAll('#backdrop, #sidebar-close')) {
  el.addEventListener('click', () => {
    menuInputEl.checked = false;
    document.body.classList.remove('show-sidebar');
  });
}


// Search suggestions for the search input, using FlexSearch
import flexsearch from 'flexsearch';
// import advanced from 'flexsearch/dist/lang/latin/advanced.min';

const index = new flexsearch.Document({
  document: {
    id: 'id',
    index: [
      {
        field: 'title',
        // tokenize: 'forward',
        tokenize: 'forward', // IMPROVE: also match partially, similar to VSCode file search?
        // encode: advanced, // IMPROVE: add a (language-specific) encoder?
        cache: true, // Note: similar to that used in https://github.com/nextapps-de/flexsearch/blob/master/demo/autocomplete.html. IMPROVE: add bounds/limit?
      },
      {
        field: 'description',
        tokenize: 'forward',
        cache: true,
      },
      {
        field: 'content',
        tokenize: 'forward',
        cache: true,
      },
    ],
  },
});

// Read the content for the search suggestions. Note: wrapped inside a function, since Hugo does not support top level await. IMPROVE: insert the data directly in this file, like the Doks theme does?
(async () => {
  // {{ $searchData := resources.Get "js/search-data.json" | resources.ExecuteAsTemplate "js/search-data.json" . }}
  // {{ if hugo.IsProduction }}
  //   {{ $searchData = $searchData | fingerprint }}
  // {{ end }}
  let data: any;

  try {
    const response = await fetch('{{ $searchData.RelPermalink }}');
    if (!response.ok) {
      throw new Error('Error loading search suggestions');
    }

    data = await response.json();
  } catch (error) {
    console.error('Error fetching the JSON file:', error);
    throw error;
  }

  for (let i = 0; i < data.length; i++) {
    index.add(i, data[i]); // Note: use the index instead of the ID as key
  }

  const searchInput = document.getElementById('search-input') as HTMLInputElement;
  const searchResults = document.getElementById('search-results') as HTMLElement;
  const searchForm = document.getElementById('search-form') as HTMLInputElement;

  document.getElementById('search-form')?.addEventListener('submit', (e) => { e.preventDefault(); });

  searchInput.addEventListener('input', () => {
    let suggestions: any[] = [];

    if (searchInput.value) {
      const results = index.search(searchInput.value, { limit: 10, suggest: true });

      // For now, only take the first match into account (match by title, or if that does not match: by description, or otherwise: by content)
      if (results.length) {
        suggestions = results[0].result.map(i => data[i]); // Note: instead of a loop with direct array element assignment, which is ignored by Svelte reactivity
      }
    }

    searchResults.replaceChildren(...suggestions.map(sug => {
      const el = document.createElement('a');
      el.setAttribute('href', sug.href);
      const title = document.createElement('span');
      title.textContent = sug.title;
      const description = document.createElement('span');
      description.textContent = sug.description;

      el.appendChild(title);
      el.appendChild(description);
      return el;
    }));
  });

  // Close the search suggestions when clicked outside the search form (which contains the input and search results)
  document.body.addEventListener('click', (event: MouseEvent) => {
    // If the clicked element is the search form or a descendant of it, clear the search suggestions
    if (!searchForm.contains(event.target as Node)) {
      searchResults.replaceChildren();
    }
  });
})();


// Keyword popovers
tippy('.keyword', {
  theme: 'light-border',
  placement: 'bottom-start',
  content: 'Laden…',
  allowHTML: true,
  interactive: true, // Do not hide the popover when hovered
  onShow(instance) {
    // Slugify the keyword to use as a URL
    const slug = instance.reference.textContent.toLowerCase()
      .normalize('NFD').replace(/[\u0300-\u036f]/g, '') // Remove diacritics
      .replace(/[^a-z0-9]/g, '-') // Replace non-alphanumeric characters with dashes
      .replace(/^-+|-+$/g, ''); // Trim leading/trailing dashes

    fetch(`/keywords/${slug}/`)
      .then((response) => response.text())
      .then((content) => {
        instance.setContent(content);
      })
      .catch((error) => {
        // Fallback if the network request failed
        instance.setContent(`Toelichting ontbreekt (${error}).`);
      });
  }
});


// Update the sidebar active item when scrolling
const sidebar = document.getElementById('sidebar');

if (sidebar) {
  const showAsActive = (targetID: string) => {
    // Clear all 'active' classes in the sidebar
    for (let el of sidebar.querySelectorAll('.current .active')) {
      el.classList.remove('active');
    }

    // Set the link thats links to the targetID as active
    if (targetID) {
      sidebar.querySelector(`.current [href$="#${targetID}"]`)?.classList.add('active');
    } else {
      // If no active page selected, set the current page as active
      sidebar.querySelector(`.current a`)?.classList.add('active'); // Note: querySelector matches the first element that matches the selector
    }
  };

  const headings = document.querySelectorAll('article h1, article h2');
  const headingIDs = Array.from(headings).map(el => el.id);
  let previousY = window.scrollY;

  let observer = new IntersectionObserver((entries) => {
    for (let { isIntersecting, target } of entries) {
      const isScrollingUp = window.scrollY < previousY;
      previousY = window.scrollY;

      if (isIntersecting) {
        // First intersecting element: show as active, then return
        showAsActive(target.id);
        return;
      }

      // Else (when this heading scrolls out of the root)...
      if (isScrollingUp) {
        // When leaving this element while scrolling up: set the heading above the current one as active
        const targetIndex = headingIDs.indexOf(target.id);
        if (targetIndex <= 0) {
          showAsActive('');
        } else {
          showAsActive(headingIDs[targetIndex - 1]);
        }
      }
    }
  }, {
    // root: null, // If unspecified or null: browser viewport
    rootMargin: '16px 0px -80%', // Corresponds to the header height, see also the scroll-margin-top in main.scss. Must be specified in pixels and/or percents. -80% used to only trigger when the heading is in the top part of the viewport
    threshold: 0, // Trigger when at least 1 pixel of the content is inside the root/viewport
  });

  for (const heading of headings) {
    observer.observe(heading);
  }
}

// Function for calendar date comparison
function isOnSameDay(date1: Date, date2: Date): boolean {
  const formatter = new Intl.DateTimeFormat('nl-NL', {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
    // timeZone: 'Europe/Amsterdam', // Note: we assume that the date is already in the correct timezone
  });

  return formatter.format(date1) === formatter.format(date2);
}

const dateOptions: Intl.DateTimeFormatOptions = {
  weekday: 'short', // 'di'
  day: '2-digit',  // '22'
  month: 'short',  // 'okt'
  year: 'numeric'  // '2024'
}

const timeOptions: Intl.DateTimeFormatOptions = {
  hour: '2-digit',  // '14'
  minute: '2-digit',  // '00'
}

const dateTimeOptions: Intl.DateTimeFormatOptions = {
  weekday: 'short', // 'di'
  day: 'numeric',  // '22'
  month: 'short',  // 'okt'
  year: 'numeric',  // '2024'
  hour: '2-digit',  // '14'
  minute: '2-digit',  // '00'
}

// Fetch the calendar items
const calendar = document.getElementById('calendar');
if (calendar) {
  fetch('https://pleio-events.core.digilab.network/')
    .then((response) => response.json())
    .then((data: Activity[]) => {
      if (calendar) {
        const html = data.map((item) => {
          const startDate = new Date(item.entity.startDate);
          const endDate = new Date(item.entity.endDate); // Note: we assume the end date is on the same day as the start date

          return `
            <a class="group border border-gray-300 rounded-md"
              href="https://digilab.pleio.nl${item.entity.url}"
              target="_blank">
              <span class="block pt-6 px-6 text-xl font-bold text-blue-900 mb-2.5">${item.entity.title}</span>

              <span class="flex items-center px-6 mb-2.5">
                <span class="shrink-0 px-3 py-1 border border-gray-300 rounded-md text-center mr-3 text-gray-500">
                  <span class="block text-xl font-bold text-gray-800">${new Intl.DateTimeFormat('nl-NL', {
            day: '2-digit',  // '22'
          }).format(startDate)}</span>
                ${new Intl.DateTimeFormat('nl-NL', {
            month: 'short',  // 'okt'
          }).format(startDate)}
                </span>

                <span class="text-sm">${isOnSameDay(startDate, endDate) ? `${new Intl.DateTimeFormat('nl-NL', dateOptions).format(startDate)}<br>
              ${new Intl.DateTimeFormat('nl-NL', timeOptions).format(startDate)} – ${new Intl.DateTimeFormat('nl-NL', timeOptions).format(endDate)}`
              : `${new Intl.DateTimeFormat('nl-NL', dateTimeOptions).format(startDate)}`}<br>
          ${new Intl.DateTimeFormat('nl-NL', dateTimeOptions).format(endDate)}
          </span>
              </span>

              ${item.entity.rsvp ?
              `<span class="flex items-center border-b border-gray-300 pb-6 px-6">
                <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8 text-gray-300 mr-2" viewBox="0 0 24 24">
                  <path fill="currentColor"
                    d="M17 3.34a10 10 0 1 1-14.995 8.984L2 12l.005-.324A10 10 0 0 1 17 3.34m-1.293 5.953a1 1 0 0 0-1.32-.083l-.094.083L11 12.585l-1.293-1.292l-.094-.083a1 1 0 0 0-1.403 1.403l.083.094l2 2l.094.083a1 1 0 0 0 1.226 0l.094-.083l4-4l.083-.094a1 1 0 0 0-.083-1.32" />
                </svg>
                ${item.entity.attendees.totalAccept} aanmeldingen</span>` : ''}

              <span class="flex py-2 items-center justify-center text-gray-600 rounded-b-md group-hover:bg-gray-100"><svg
                  xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 text-blue-400 mr-2" viewBox="0 0 24 24">
                  <path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="m5 12l5 5L20 7" />
                </svg>
                ${item.entity.rsvp ? 'Meld je aan' : 'Lees meer'}</span>
            </a>
        `;
        }).join('');

        if (html) {
          calendar.innerHTML = `<p class="md:col-span-2 lg:col-span-3">De volgende evenementen zijn gepland:</p> ${html}`;
        } else {
          calendar.innerHTML = '<p class="md:col-span-2 lg:col-span-3">Er zijn geen evenementen gepland.</p>';
        }
      }
    })
    .catch((error) => {
      console.error('Error fetching the calendar items:', error);
    });
}
