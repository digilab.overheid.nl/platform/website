module gitlab.com/digilab.overheid.nl/ecosystem/digilab-website

go 1.22.1

require (
	gitlab.com/datastelsel.nl/federatief/puzzels v0.0.0-20240703073840-439e07275fa4 // indirect
	gitlab.com/digilab.overheid.nl/ecosystem/dcat-v2-validator/docs v0.0.0-20240212124937-d1cc7409dbb7 // indirect
	gitlab.com/digilab.overheid.nl/ecosystem/digid-proxy v0.0.0-20240611155230-8c2903c424e6 // indirect
	gitlab.com/digilab.overheid.nl/ecosystem/it-hygiene-validator v0.0.0-20240212124702-8dffe31431f8 // indirect
	gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo v0.0.0-20240624121239-955879cb0d6e // indirect
	gitlab.com/digilab.overheid.nl/ecosystem/terraform-modules/azure-generic-ipv6-proxy v0.0.0-20240606142641-7afd1ae10f11 // indirect
	gitlab.com/digilab.overheid.nl/platform/fsc-policy-builder v0.0.0-20240424074716-e92d3de7e6c1 // indirect
	gitlab.com/digilab.overheid.nl/research/event-sourcing/docs v0.0.0-20240212125435-afb712e8c261 // indirect
)
