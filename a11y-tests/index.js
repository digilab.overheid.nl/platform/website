const { AxePuppeteer } = require('@axe-core/puppeteer');
const puppeteer = require('puppeteer-core');
const { createHtmlReport } = require('axe-html-reporter');
const xml2js = require('xml2js');


(async () => {
  // Fetch the sitemap and parse its URLs
  let sitemapContent;
  try {
    const response = await fetch('https://digilab.overheid.nl/sitemap.xml');

    if (!response.ok) {
      throw new Error('Network response was not ok');
    }

    sitemapContent = await response.text(); // or response.json() for JSON files
  } catch (error) {
    console.error('Error fetching the remote file:', error);
  }

  let urls;
  xml2js.parseString(sitemapContent, (error, result) => {
    if (error) {
      console.error(error);
      return;
    }
  
    // Extract URLs from the parsed XML
    urls = result.urlset.url.map((urlItem) => urlItem.loc[0]);
  });


  const browser = await puppeteer.launch({
    headless: 'new',
    executablePath: process.env.PUPPETEER_EXECUTABLE_PATH || '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
    args: [
      '--no-sandbox',
      '--disable-gpu', // See https://github.com/puppeteer/puppeteer/issues/13048
    ],
  });
  const page = await browser.newPage();

  let hasSeriousOrCriticalViolation = false;

  // for (const theme of ['dark', 'light']) {
  for (const theme of ['light']) {
    // // For the light theme, set the theme in localStorage first. Note: we assume the browser is already on the same domain. IMPROVE: set before navigating, see https://stackoverflow.com/a/56141229
    // if (theme === 'light') {
    //   await page.evaluate(() => {
    //     localStorage.setItem('theme', 'light');
    //   });
    // }

    for (const url of urls) {
      console.log('checking:', url);
      await page.goto(url);

      try {
        const results = await new AxePuppeteer(page).analyze();

        if (results.violations.length) {
          createHtmlReport({
              results: results,
              options: {
                reportFileName: `${url.replace(/[^a-zA-Z0-9]/g, ' ').trim().replace(/\s+/g, '-')}-${theme}.html`,
                outputDir: 'reports',
                projectKey: 'Digilab website',
                // customSummary: '',
                outputDirPath: '/builds/digilab.overheid.nl/ecosystem/digilab-website',
                // doNotCreateReportFile: true,
              },
          });

          for (const violation of results.violations) {
            if (violation.impact == 'serious' || violation.impact == 'critical') {
              hasSeriousOrCriticalViolation = true;
              break
            }
          }
        } else {
          console.log('passed!');
        }
      } catch (e) {
        console.log('error:', e);
      }
    }
  }

  await browser.close();

  // In case of a serious or critical error, let the pipeline fail
  if (hasSeriousOrCriticalViolation) {
    console.log('Found a serious or critical violation, letting the pipeline fail.');
    process.exit(1); // Exit with a status code of 1
  }
})();
