---
title: "Privacyverklaring Digilab"
description: ""
date: 2023-09-18T09:35:34+02:00
---

Privacy & security wordt integraal meegenomen in het ontwerp en onderhoud van onze applicaties. Deze privacyverklaring moet worden gelezen in aanvulling op en in samenhang met de algemene privacyverklaring van het Ministerie van Binnenlandse Zaken en Koninkrijksrelaties (BZK).

Deze algemene privacyverklaring gaat over het gebruik van uw persoonsgegevens door het Digilab. Digilab is onderdeel van BZK. De verklaring geldt voor alle gevallen waarin uw persoonsgegevens door, of in opdracht van BZK worden gebruikt.

Wij respecteren uw privacy en gaan zorgvuldig om met uw persoonsgegevens. Hierbij houden wij ons aan de Algemene verordening gegevensbescherming (AVG).

Vanwege nieuwe wetgeving of andere ontwikkelingen past BZK regelmatig haar processen aan. Dit kan ook wijzigingen inhouden ten aanzien van het verwerken van persoonsgegevens. Het is daarom raadzaam om regelmatig de algemene en deze specifieke privacyverklaring te raadplegen. Aan het einde van de verklaring staat aangegeven wanneer deze voor het laatst is aangepast.

### 1. Beheer
De websites [digilab.overheid.nl](https://digilab.overheid.nl) en [digilab.overheid.nl/chat](https://digilab.overheid.nl/chat) staan onder het beheer van Digilab. De contactgegevens van Digilab staan op [digilab.overheid.nl/contact](https://digilab.overheid.nl/contact)

### 2. Wat zijn persoonsgegevens?
Een persoonsgegeven is informatie die direct over iemand gaat of informatie die naar deze persoon te herleiden is. Voorbeelden van persoonsgegevens zijn adressen, telefoonnummers en e-mailadressen.

### 3. Waarvoor gebruikt Digilab persoonsgegevens? 
Digilab is de verwerkingsverantwoordelijke voor de hierna genoemde verwerkingen. 
Hieronder wordt meer uitleg gegeven over de verwerking, inclusief het doel, de grondslag en andere belangrijke informatie.

### Doel [digilab.overheid.nl](https://digilab.overheid.nl)
Het doel van het verwerken van persoonsgegevens voor digilab.overheid.nl: Het opnemen van contact om meer informatie te verstrekken over Digilab. 

**Grondslag** 
Digilab verwerkt deze persoonsgegevens op basis van gerechtvaardigd belang.

**Categorieën Persoonsgegevens**
Vrijwillig afgegeven gegevens kunnen bestaan uit : 
-Naam
-Functie
-Organisatie
-E-mailadres
-Telefoonnummer 

**Ontvangers, doorgifte, geen profiel en geen geautomatiseerde besluitvorming**
Bij digilab.overheid.nl is er sprake van ontvangers van persoonsgegevens. Door het invullen van de contact-chatbot, worden gegevens doorgezet naar digilab.overheid.nl/chat. Digilab deelt de gegevens van de bezoekers niet met derde partijen. De persoonsgegevens van Digilab worden niet buiten de Europese Economische Ruimte (EER) verwerkt. Er worden geen profielen opgesteld. Er vindt geen geautomatiseerde besluitvorming plaats.

**Bewaartermijnen**
De gegevens worden bewaard zolang noodzakelijk voor het doel van de verwerking.

###	Doel [digilab.overheid.nl/chat](https://digilab.overheid.nl/chat)
Het doel van het verwerken van persoonsgegevens voor Digilab : digilab.overheid.nl/chat is een samenwerkingsplatform voor professionals om kennis uit te wisselen. Om deel te kunnen nemen aan dit platform is een e-mailadres nodig om in te loggen. Eénmaal ingelogd kunnen deelnemers elkaar benaderen en berichten (met vrije tekstvelden) uitwisselen. 

**Grondslag**
Digilab verwerkt deze persoonsgegevens op basis van gerechtvaardigd belang.

**Categorieën Persoonsgegevens**
-E-mailadres
-Andere informatie die vrijwillig is verstrekt (naam, telefoonnummer, foto’s)

**Geen ontvangers, geen doorgifte, geen profiel en geen geautomatiseerde besluitvorming**
Digilab deelt de gegevens van de bezoekers niet met andere partijen. De persoonsgegevens van Digilab.overheid.nl/chat worden niet buiten de Europese Economische Ruimte (EER) verwerkt. Er worden geen profielen opgesteld. Er vindt geen geautomatiseerde besluitvorming plaats.

**Bewaartermijnen**
De gegevens worden bewaard zolang noodzakelijk voor het doel van de verwerking.

### 4. Cookies

Cookies zijn kleine tekstbestanden die op je apparaat worden geplaatst wanneer je onze website bezoekt. Onze websites maken gebruik van cookies en vergelijkbare technologieën om de functionaliteit en prestaties te verbeteren en om je een gepersonaliseerde ervaring te bieden. Onze website gebruikt geen tracking- of marketingcookies. Voor al deze cookies geldt dat u ze altijd handmatig van je computer kunt verwijderen. 

### 5. Contact
Mocht u nog vragen hebben, die niet in dit document, noch in het document met algemene informatie, worden beantwoord, of als u om een andere reden contact wil opnemen in het kader van privacy of gegevensbescherming, neem dan contact op via digilab.overheid.nl/contact.

### 6. Uw rechten uitoefenen en klachten
Als burger of belanghebbende kunt u uw rechten bij Digilab uitoefenen. Op grond van de AVG kunt u ons verzoeken om:
-inzage in uw gegevens
-correctie en verwijdering van uw gegevens
-beperking van het gebruik van uw gegevens

Er kunnen redenen zijn waarom uw verzoek niet kan worden ingewilligd. Als dat het geval is dan zullen wij dit goed aan u uitleggen.
Voor meer informatie over het uitoefenen van uw rechten, kunt u contact met ons opnemen via [digilab.overheid.nl/contact](https://digilab.overheid.nl/contact)
Wanneer u een klacht heeft over het handelen van Digilab, dan heeft u het recht om klacht in te dienen bij de privacy toezichthouder, de Autoriteit Persoonsgegevens.

### 7. Wijzigingen
*Deze privacyverklaring is opgesteld op: 5 december 2023*
