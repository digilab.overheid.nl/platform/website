---
title : "Static BRP test data"
description: "Static BRP testdata deployed by Digilab"
date: 2023-06-19T08:47:36+00:00
---

## Inleiding

[HaalCentraal](https://haalcentraal.pleio.nl/) is een initiatief waarmee basisgegevens rechtstreeks bij de landelijke registraties kunnen worden opgevraagd. HaalCentraal biedt [verschillende APIs](https://vng-realisatie.github.io/Haal-Centraal/aansluiten-op-apis) aan, onder andere de [BRP API Personen](https://brp-api.github.io/Haal-Centraal-BRP-bevragen/) *(BRP = Basisregistratie Personen)*.

HaalCentraal biedt een testversie van deze API aan die lokaal gestart kan worden. Ook is de testversie online beschikbaar, dit vereist echter de aanvraag van een API key. Voor extra gebruiksgemak biedt Digilab deze testversie van de API openbaar aan. Deze is bereikbaar via het endpoint `https://brp-api-mock.apps.digilab.network/haalcentraal/api/brp/personen`.


## Voorbeeld

De API kan als volgt bereikt worden:

```sh
curl -X POST 'https://brp-api-mock.apps.digilab.network/haalcentraal/api/brp/personen' -H 'Content-Type: application/json' -d '{
    "type": "RaadpleegMetBurgerservicenummer",
    "burgerservicenummer": ["999993653"],
    "fields": ["burgerservicenummer", "naam"]
}'
```

Dit geeft (na formattering) de volgende response:

```json
{
    "type": "RaadpleegMetBurgerservicenummer",
    "personen": [
        {
            "burgerservicenummer": "999993653",
            "naam": {
                "aanduidingNaamgebruik": {
                    "code": "E"
                },
                "voornamen": "Suzanne",
                "geslachtsnaam": "Moulin",
                "voorletters": "S.",
                "volledigeNaam": "Suzanne Moulin"
            }
        }
    ]
}
```


## Meer informatie

Voor uitgebreider gebruik, zie de [API specificatie](https://brp-api.github.io/Haal-Centraal-BRP-bevragen/v2/redoc).

Zie ook het [overzicht van beschikbare testpersonen / -BSN's](https://github.com/BRP-API/Haal-Centraal-BRP-bevragen/blob/master/docs/v1/getting-started.md#testpersonen).

Voor een vergelijkbare API met meer personen, random gegenereerd, zie de [Digilab Synthetic Data Generator](/docs/testvoorzieningen/synthetic-data-generator/) (documentatie in het Engels).
