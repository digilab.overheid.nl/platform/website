---
title : "Testvoorzieningen"
description: "Testvoorzieningen aangeboden door Digilab."
date: 2023-06-19T08:47:36+00:00
weight: 2
---

![](testvoorzieningen.png)

Digilab biedt de volgende testvoorzieningen aan:

<div class="grid sm:grid-cols-3 gap-4">
  <a class="block p-4 border border-slate-200 rounded-md no-underline text-inherit font-normal hover:border-slate-400" href="/docs/testvoorzieningen/static-brp-test-data/">
    <strong class="block text-blue-600 font-semibold">Statische BRP testdata</strong>
    Testdata voor de Basisregistratie Personen volgens de HaalCentraal API, met een vast aantal test-BSNs.
  </a>

  <a class="block p-4 border border-slate-200 rounded-md no-underline text-inherit font-normal hover:border-slate-400" href="/docs/testvoorzieningen/synthetic-data-generator/">
    <strong class="block text-blue-600 font-semibold">Synthetische BRP testdata</strong>
    Testdata voor de Basisregistratie Personen volgens de HaalCentraal API, met een groot aantal personen die random zijn gegenereerd op basis van CBS-statistieken.
  </a>

  <a class="block p-4 border border-slate-200 rounded-md no-underline text-inherit font-normal hover:border-slate-400" href="/docs/testvoorzieningen/digid-proxy/">
    <strong class="block text-blue-600 font-semibold">DigiD proxy</strong>
    Specifiek voor projecten op Digilab: een proxy waarmee eenvoudig de DigiD-login voor applicaties (niet in productie) kan worden opgezet.
  </a>
</div>
