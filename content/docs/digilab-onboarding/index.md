---
title: Digilab platform onboarding
draft: false
date: 2024-03-26T12:52:22.032Z
description: Als je project is goedgekeurd door het Digilab Portfolio Team, dan
  kun je gebruik maken van het platform dat Digilab biedt voor bijvoorbeeld hosting van
  je applicatie. Hier lees je wat te de nemen stappen zijn, en vind je links
  naar handige documentatie en tools.
weight: 1
---

![](onboarding-docs.png)

Als je project is goedgekeurd door het Digilab Portfolio Team, dan kun je gebruik maken van het platform dat Digilab biedt voor bijvoorbeeld hosting van je applicatie. Hier lees je wat te de nemen stappen zijn, en vind je links naar handige documentatie en tools.

## Omgeving

Digilab biedt een (Haven compliant) Kubernetes omgeving aan. Deze omgeving is gehost in Azure, maar is zo ingericht dat het cloud-agnostisch is. Alle workloads draaien binnen het Kubernetes cluster en hebben daardoor geen cloud provider lock-in. Om deze reden is het niet mogelijk gebruik te maken van Azure specifieke services - bij Digilab geven we altijd de voorkeur aan het gebruik van opensourcealternatieven die draaien binnen Kubernetes.

## Mattermost

Voor onboarding is er in de Mattermost-omgeving van Digilab een kanaal genaamd [digilab infra](https://digilab.overheid.nl/chat/digilab/channels/digilab-infra). Hier kun je je melden als je gebruik wilt maken van de infrastructuur van Digilab, voor bijvoorbeeld het hosten van een applicatie of het trainen van een ML-model. Iemand van team Digilab zal met jou de technische eisen doorlopen en de omgeving inrichten. Er zal bijvoorbeeld worden gecheckt of je workload voldoet aan de [technische vereisten](https://digilab.overheid.nl/technische-vereisten-deelname-digilab/).

## Identiteiten

Digilab gebruikt Pleio als identity provider, zorg dus dat je hier [een account](https://account.pleio.nl/register/) hebt. Wij koppelen Pleio aan Authentik, van waaruit de permissies per groep en/of gebruiker worden beheerd (bijvoorbeeld toegang tot een Kubernetes cluster).

## Clustertoegang

Vraag via bovenstaade Mattermost-kanaal toegang aan voor jouw team. Installeer vervolgens de [pinniped CLI](https://pinniped.dev) en je kunt inloggen met je Pleio-account op het cluster. Je hebt alleen toegang tot de namespaces van jouw team.

## CI/CD en artifacts

Digilab biedt geen omgeving voor het draaien van CI/CD pipelines. We raden hiervoor zelf Tekton CI aan, maar je kunt ook gebruik maken van Gitlab CI of een andere CI/CD tool. Artifacts kun je opslaan in een registry naar keuze, zoals Gitlab Container Registry, Docker Hub of Github Container Registry.

## GitOps repository
Digilab maakt gebruik van het GitOps principe, waarbij er declaratief gedifinieerd wordt wat je wilt bereiken, en dit opslaat in een Git repository. Deze repository wordt door FluxCD gemonitord en zorgt ervoor dat de gewenste situatie in de cluster wordt bereikt. Digilab kan deze repository voor je aanmaken en behoren (in de [digilab.overheid.nl](https://gitlab.com/digilab.overheid.nl) Gitlab.com groep), maar je kunt deze ook zelf beheren. De repository moet via het internet bereikbaar staan en je moet een deploy key van Flux toevoegen aan de repository.

## Monitoring

Digilab levert een complete Grafana-instantie waarmee je je applicaties kunt monitoren. Momenteel worden er alleen logs ondersteund, maar worden metrics binnenkort ook toegevoegd. Traces staan op de roadmap.

### Grafana
Elk team krijgt een eigen Grafana-instantie. Je kunt deze via het internet bereiken, de URL wordt je door Digilab verschaft. Je kunt inloggen via SSO (Pleio). In deze instantie heb je alle vrijheid voor het maken van dashboards, alerts en filters; de instantie is namelijk dedicated voor jouw team.

![](grafana-home.png)

### Logging
Zodra je inlogt zie je een standaard datasource voor de logs (Loki). Op de Explore pagina kun je in deze logs zoeken en filteren op bijv. namespace of containernaam. Je vindt alleen logs uit namespaces die behoren tot jouw team. Je kunt ook zelfs dashboards aanmaken zodat je direct de informatie ziet die jij wilt.

Het is mogelijk om extra metadata met logs mee te sturen, neem hiervoor contact op met het Digilab team.

![](grafana-explore.png)

### Metrics
In ontwikkeling.

### Traces
In ontwikkeling.

## Secret encryption
To achieve full GitOps, all resources should be in Git. An application might need secrets, such as API keys to 
communicate with external services. It is a bad practice though, to store secrets plaintext in Git. Therefore, secrets 
are asymmetrically encrypted using [SOPS](https://github.com/getsops/sops) + [age](https://github.com/FiloSottile/age).

sops is an editor of encrypted files that supports YAML, JSON, ENV, INI and BINARY formats and encrypts with AWS KMS, GCP KMS, Azure Key Vault, age, and PGP.

age is a simple, modern and secure file encryption tool, format, and Go library.

age is natively supported by SOPS.

When installing Flux, a keypair is generated and directly stored in the cluster. This is the only place where it's stored, so we store a copy in Azure Keyvault, allowing is to recreate a cluster from scratch with state. Secrets are encrypted locally with the public key, stored in Git, and decrypted by Flux on the fly.

When creating a new tenant, the keypair is stored in Git, but encrypted with the public key from the previous paragraph. This allows Flux to decrypt it and store it in the cluster. When a tenant wants to add a secret, that tenant can use their own public key to encrypt the secret and store it in Git. Flux will automatically select the correct private key, decrypt the secret and store it in the cluster, available for use by the tenant. A dedicated Kubernetes service account for the tenant is used to apply the resources. This service account only has access to its own namespace.

Public keys are stored in [this repository](https://gitlab.com/digilab.overheid.nl/platform/flux/core/-/tree/main/sops-public-keys?ref_type=heads). To encrypt a Kubernetes secret using such a public key, use the following command:

```bash
sops \
  --encrypt \
  --encrypted-regex '^(data|stringData)$' \
  --age "[age-pub-key]" \
  --in-place \
  my-kubernetes-secret.yaml
```

The `encrypted-regex` only encrypts the secret values, not other metadata, which keeps the Kubernetes secret readable and identifiable. You can safely commit the encrypted secret. When Flux reconciles the updated manifests, it will be able to automatically decrypt the secret and apply it.

## Documentatie

* [Jouw applicatie lokaal in Kubernetes draaien met k3d](https://gitlab.com/digilab.overheid.nl/platform/flux/core/-/blob/main/docs/run-app-locally.md?ref_type=heads)
* [Jouw Tekton CI pipeline lokaal draaien in Kubernetes](https://gitlab.com/digilab.overheid.nl/platform/flux/core/-/blob/main/docs/run-tekton-locally.md?ref_type=heads)
* [Team toevoegen op Digilab platform](https://gitlab.com/digilab.overheid.nl/platform/flux/docs/-/blob/main/pages/add-a-new-team.md?ref_type=heads)
* [Applicatie toevoegen op Digilab platform](https://gitlab.com/digilab.overheid.nl/platform/flux/docs/-/blob/main/pages/add-a-new-app.md?ref_type=heads)
* [Technische vereisten applicatie](https://digilab.overheid.nl/technische-vereisten-deelname-digilab/)
* [Omschrijving Fieldlab-omgeving](https://gitlab.com/digilab.overheid.nl/platform/flux/tenant-digilab-fieldlab/-/blob/main/README.md?ref_type=heads)
* [Architectuur van platform en gebruik van tooling (o.a. Flux)](https://gitlab.com/digilab.overheid.nl/platform/terraform/infrastructure/-/blob/main/docs/index.md?ref_type=heads)

## Handige links/tools
* [k9s](https://k9scli.io): CLI tool voor het monitoren van Kubernetes workloads
* [kubectx/kubens](https://github.com/ahmetb/kubectx): CLI tool voor het switchen tussen verschillende Kubernetes clusters en namespaces
