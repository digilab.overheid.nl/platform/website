---
title: "Wat biedt Digilab?"
description: ""
date: 2023-05-23T10:35:34+02:00
description: Waarvoor Digilab er is, wat Digilab wel en niet biedt, wat we doen, Digilab-faciliteiten, ondersteuning en community.
weight: 1
---

### Digilab is er voor
* innovatieve projecten
* die tot doel hebben om slim, verantwoord en met moderne technieken data te delen
* binnen de overheid.

### Digilab biedt
* een ontwikkel- en testomgeving waarin deze projecten
* samen als één community
* kunnen meebouwen aan het Federatief Datastelsel.

### Dat doen we
* met beproevingen
* die nieuwe standaarden en afspraken of best practices kunnen opleveren,
* of de toepasbaarheid van bestaande standaarden en afspraken kunnen verbeteren.

### Digilab-faciliteiten

* Een Kubernetes-infrastructuur voor ontwikkeling en testen, Haven-compliant
* OpenKAT kwetsbaarhedenanalysetool
* Online samenwerkomgeving (online community, zie de [contactpagina](http://localhost:1313/contact/) voor meer details)
* Fysieke ruimte om te vergaderen in het midden van het land

### Het Digilab-team kan ondersteunen bij:

* Overzetten van koppelingen naar testomgeving
* Zoeken naar koppelingen die herbruikbaar/standaardiseerbaar zijn
* Het verzamelen of creëren van (synthetische) testdata
* Verbindingen leggen met andere projecten op Digilab
* Het opzetten van een proef door:
- Deelnemen aan brainstormsessies
- Meedenken over technieken en concepten die voor het project interessant kunnen zijn
- Deelnemen aan en mede-organiseren van hackathons
- Het vinden van meewerkende overheidspartijen
- Verbindingen te leggen met andere Digilab-projecten

### De Digilab-community biedt:
* Kans om API-capabilities afstemmen met andere API-bouwers binnen overheid
* Synergie met andere innovatieve projecten waar Digilab mee samenwerkt
* Kennisdeling rond de onderwerpen waar de Digilab-projecten aan werken
* Een podium om oplossingen en geleerde lessen breed binnen de overheid te delen

### Op termijn gaat Digilab mogelijk ook werken aan:
* Technische randvoorwaardelijke zaken die projecten buiten scope willen plaatsen en die Digilab wel kan oppakken (zoals technisch interoperabele wijze voor authenticate en autorisatie)
* Testdata FDS-registraties
* Testkoppelingen met FDS-registraties

### Wat biedt Digilab niet:
* Financiering van het project (behalve de door Digilab geboden diensten hierboven in natura)
* Governance van het project
* Ontwikkelcapaciteit voor het project, behalve voor de hierboven genoemde ondersteuning
* Testomgeving voor productie

## Werkwijze(r)
In dit overzicht laten we zien wat we jou als deelnemer aan Digilab te bieden hebben én wat we van jou verwachten: [Werkwijze(r) Digilab](/over/werkwijzer)
