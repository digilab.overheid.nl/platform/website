---
title: "Werkwijze(r) Digilab"
description: ""
date: 2023-12-08T10:35:34+02:00
weight: 3
layout: werkwijzer
---

Deelnemers aan Digilab committeren zich aan deze Digilab-werkwijze(r):

## Fair use principe
Het project kan gebruik maken van Digilab en alle bijbehorende faciliteiten op basis van “fair use”. 

## Openheid en herbruikbaarheid
- De softwarecomponenten van het project zijn open van karakter en vrij herbruikbaar (vanwege de nuances in vrije herbruikbaarheid wordt dit tijdens de intake besproken).
- Het project publiceert alle relevante documentatie voor het gebruik van de componenten.
- Digilab zelf gebruikt de EUPL-licentie. 

## Faciliteiten:
- Een Kubernetes (cloud)infrastructuur voor ontwikkeling en testen, Haven compliant
- OpenKAT kwetsbaarhedenanalysetool
- Online samenwerkomgeving 
- Fysieke ruimte om te vergaderen in het midden van het land.

## Beschikbaarheid
- Beschikbaarheid van het platform en faciliteiten van Digilab is op basis van best effort.
- Digilab zal de past performance  publiceren. De link naar deze pagina zal nog toegevoegd worden. 
- Bij voorziene structurele verandering in de beschikbaarheid van het Digilab-platform informeert Digilab projecten uiterlijk drie maanden vantevoren.

## Technische vereisten
- De technische vereisten voor deelname aan Digilab zijn [hier](/technische-vereisten-deelname-digilab/) gepubliceerd. 

## Technische wensen
- Bekend met CI/CD. Digilab kan faciliteren met Gitlab, maar eigen varianten zijn ook toegestaan
- Bekend met configuratie in code, waarmee Digilab bijvoorbeeld RBAC (Role Based Access Control) inricht
- Containers hebben ieder een unieke rol, bijvoorbeeld webserver en niet tevens redis cache
- Containers zijn in parallel te draaien (High Availability)
- Containers schrijven niet naar eigen disk, maar naar een Volume of ObjectStorage
- Bonus: automatische patching van dependencies (bijvoorbeeld Renovatebot), bekend met OIDC, bekend met Tailscale

## Technische en architectuurreviews
- Iedere proef werkt mee aan ten minste één tussentijdse review van de techniek en architectuur.
- Naar behoefte en in overleg kunnen er meer reviews worden georganiseerd.
- Het project geeft bij de onboarding aan wat een logische mijlpaal zou zijn om een of meer reviews te organiseren.
- De proeven geven zelf aan op welke punten zij behoefte hebben aan feedback.
- Deel van de review is in ieder geval of de gekozen richting past binnen een Federatief Datastelsel.
- Na afloop van de proef volgt een evaluatie en presentatie bij Digilab en het project Realisatie FDS.
- Het project Realisatie FDS zal met input van de uitvoerders van de proef een Architectural Decision Record opstellen of aanvullen met de geleerde lessen uit de proef voor een Federatief Datastelsel.


## Aanvullende ondersteuning
Het Digilab-team kan ondersteunen bij: 
- Overzetten van koppelingen naar testomgeving
- Zoeken naar koppelingen die herbruikbaar/standaardiseerbaar zijn 
- Het verzamelen of creëren van (synthetische) testdata
- Verbindingen leggen met andere projecten op Digilab

Als deze aanvullende ondersteuning gewenst is, stemt het project af met Digilab. De ondersteuning is eveneens op basis van best-effort en onder voorbehoud van beschikbare tijd en menskracht. Communicatie over aanvullende ondersteuning verloopt via de product owner op [Mattermost](https://digilab.overheid.nl/chat/digilab).

## Compliance
- Projecten werken onder verantwoordelijkheid van een overheidsorganisatie (te vinden op [organisaties.overheid.nl](https://organisaties.overheid.nl/). Dit laat onverlet dat deze organisatie samen kan werken met andere partijen, die toegang krijgen tot Digilab onder hun verantwoordelijkheid. Het project informeert Digilab over de partijen waar zij mee samenwerken in het project.
- Het project werkt binnen Digilab alleen met openbare gegevens of testgegevens, zodat er geen persoons- of anderszins vertrouwelijke gegevens op de omgeving komen te staan. 
- Het project committeert zich eraan binnen de Digilab-omgeving te werken met de daarin reeds geïmplementeerde (architectuur-)afspraken en standaarden. Deze zijn hier te vinden
- Deelnemers verbinden zich aan het zelf up to date houden van hun software. Software die de deelnemer niet langer onderhoudt, zal deelnemer zelf verwijderen. 

## Communicatie 
Het project werkt mee aan communicatie vanuit Digilab door onder meer:
- Mee te werken aan artikelen die voortgang, successen en lessons learned delen. 
- Demo's te geven in Digilab-demo's om behaalde resultaten te tonen. 
- Planning af te stemmen en informatie over voortgang van de beproeving te delen.
Projectinformatie 
- Informatie over het project wordt gepubliceerd via de communicatiekanalen van Digilab, zoals de website en Mattermost. Dit gebeurt in afstemming met de projecten. Het project verleent zijn medewerking aan deze publicaties en het up to date houden daarvan. 
Community-deelname
- Digilab biedt een community voor projecten  om hun kennis te delen. Het project neemt hier actief aan deel.

## Financiën
- Het project draagt zelf de kosten voor ontwikkeling, beheer en onderhoud van de binnen het eigen ‘subdomein’ van het platform op Digilab. 
- Digilab draagt in elk geval tot en met december 2025 de kosten van infrastructuur en tooling.
- Voor financiering na 2025 komt een model, in afstemming met alle Digilab-deelnemers.

## Digilab-off-boarding project

Wanneer een project is afgerond / stopt bij Digilab wordt een off-boarding proces gestart. Dit houdt in dat we in een/enkele sessie(s) de volgende punten afstemmen en/of regelen voor zover van toepassing:

### Borging Kennis
- Het project beschrijft conclusies over de voor FDS en Digilab relevante oplossingen waaraan gewerkt is (geordend volgens de puzzellijst).
- Digilab organiseert een sessie met project FDS om op basis van de conclusies van het project, gezamenlijk vast te stellen of de oplossingen binnen een FDS passen.
- FDS publiceert deze conclusies, voor zover relevant, bij de betreffende bouwstenen/puzzels op de FDS samenwerkomgeving en Digilabsite.
- Het project spreekt met Digilab een datum af waarop het Mattermostkanaal en/of -team gesloten kan worden.
- Het project geeft aan waar en hoe de kennis van de proef beschikbaar blijft.
- Het project geeft Digilab en FDS feedback over de samenwerking.

### Afspraken borging Techniek t.a.v.
- Beschikbaar houden op omgeving Digilab voor demo's. Default is dat de omgeving wordt uitgeschakeld en voor demo’s eenvoudig en snel tijdelijk zelf aangezet kan worden.
- Beschikbaarheid voor toekomstige Digilab-proeven wat betreft: componenten, openbare Git en ondersteuning bij gebruik componenten of code
- Afspraken over beschikbaar houden testdata

### Communicatie 
- Afspraken over publicaties
- Projectresultaten in artikel/presentatie (Digilab meet-up)/webinar
- Projecttekst site update en/of verplaatsen naar archief

### Organisatie / Lidmaatschap Digilab
- Afsluitend moment in Digilab Meet-up
- Afspraken t.a.v. nog uitnodigen voor community-evenementen / fieldlabs
- Stoppen toegang fysieke Digilab-ruimte (Dev.loer)

### Vervolgstappen
- Digilab en FDS spreken met het project af, waar van toepassing, hoe de resultaten verder gebracht kunnen worden. Bijvoorbeeld via het project FDS, in begeleiding naar standaardisatie, of een vervolgproef.
- In geval van een vervolgproject die de oplossing naar een productie-omgeving gaat brengen – worden afspraken gemaakt over eventueel delen van kennis/ervaringen.

## Versionering Werkwijze(r) 
- Aanvullingen/aanpassingen van de Werkwijze(r) zullen met de deelnemers worden gedeeld. 
- Bij problemen om te conformeren zal het project contact zoeken met Digilab om tot een oplossing te komen.

*versie 1.03, 25 september 2024*
