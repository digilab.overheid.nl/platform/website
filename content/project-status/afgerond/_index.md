---
title: "Projecten op Digilab"
menus:
  main:
    name: Afgerond
    parent: Projecten
excludeFromSearch: true
sitemap:
  disable: true
---

Digilab werkt aan verschillende projecten. Dat kunnen projecten zijn die een overheidsorganisatie in samenwerking met ons uitvoert, maar we hebben ook een goed gevulde backlog met eigen beproevingen.
