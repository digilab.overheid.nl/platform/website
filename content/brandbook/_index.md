---
title: "Brandbook"
description: ""
date: 2024-08-23T09:35:34+02:00
excludeFromSearch: true
sitemap:
  disable: true
---

# Logo

## Informatie

<div class="grid grid-cols-3 gap-4">
<div class="col-span-2">
Het logo van Digilab:

Erlenmeyer met opborrelende binaire code.
De code is `00101010`, ofwel 42: het antwoord op alles.

Klein formaat: geen stippen in de 0’s.
</div>

<div class="-mt-20">
<div class="h-40 mb-8 bg-contain bg-center bg-no-repeat" style="background-image: url(/img/logo-icon.svg)"></div>

<div class="h-20 mb-8 bg-contain bg-center bg-no-repeat" style="background-image: url(/img/logo-icon.svg)"></div>

<div class="h-10 bg-contain bg-center bg-no-repeat" style="background-image: url(/img/logo-icon.svg)"></div>
</div>
</div>

<p class="mb-40">
<a class="inline-flex items-center text-lg font-medium no-underline text-white bg-blue-600 hover:bg-blue-700 rounded-md px-6 py-2 transition-colors" href="/uploads/digilab-brandbook.zip">
<svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 -ml-1 mr-2.5" viewBox="0 0 24 24"><path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 17v2a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-2M7 11l5 5l5-5m-5-7v12"/></svg>
Download logopack</a>
</p>


## Formaat

<div class="grid grid-cols-2 gap-4">
<div>
<div class="h-40 mb-8 bg-contain bg-center bg-no-repeat" style="background-image: url(/img/logo-basic.svg)"></div>

<p>Klein formaat: geen stippen in de 0’s.</p>
</div>

<div class="-mt-14">
<div class="h-20 mb-8 bg-contain bg-center bg-no-repeat" style="background-image: url(/img/logo-basic.svg)"></div>

<div class="h-[3.75rem] mb-8 bg-contain bg-center bg-no-repeat" style="background-image: url(/img/logo-basic.svg)"></div>

<div class="h-10 bg-contain bg-center bg-no-repeat" style="background-image: url(/img/logo-basic.svg)"></div>
</div>
</div>

<p class="mb-40">
<a class="inline-flex items-center text-lg font-medium no-underline text-white bg-blue-600 hover:bg-blue-700 rounded-md px-6 py-2 transition-colors" href="/uploads/digilab-brandbook.zip">
<svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 -ml-1 mr-2.5" viewBox="0 0 24 24"><path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 17v2a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-2M7 11l5 5l5-5m-5-7v12"/></svg>
Download logopack</a>
</p>


## Plaatsing

<div class="grid grid-cols-3 gap-4 text-center mb-10">
<div><div class="h-44 bg-contain bg-center bg-no-repeat mb-2" style="background-image: url(/img/logo-basic.svg)"></div> Basisversie</div>
<div><div class="h-44 bg-contain bg-center bg-no-repeat mb-2" style="background-image: url(/img/logo-stacked.svg)"></div> Alternatief: gestapeld</div>
<div><div class="h-44 bg-contain bg-center bg-no-repeat mb-2" style="background-image: url(/img/logo-stacked-compact.svg)"></div> Alternatief: gestapeld compact</div>
</div>

<p class="mb-40">
<a class="inline-flex items-center text-lg font-medium no-underline text-white bg-blue-600 hover:bg-blue-700 rounded-md px-6 py-2 transition-colors" href="/uploads/digilab-brandbook.zip">
<svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 -ml-1 mr-2.5" viewBox="0 0 24 24"><path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 17v2a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-2M7 11l5 5l5-5m-5-7v12"/></svg>
Download logopack</a>
</p>


## Slogan

<div class="grid grid-cols-3 gap-4 text-center mb-10">
<div><div class="h-44 bg-contain bg-center bg-no-repeat mb-2" style="background-image: url(/img/logo-tagline.svg)"></div> Basisversie</div>
<div><div class="h-44 bg-contain bg-center bg-no-repeat mb-2" style="background-image: url(/img/logo-stacked-tagline.svg)"></div> Alternatief: gestapeld</div>
<div><div class="h-44 bg-contain bg-center bg-no-repeat mb-2" style="background-image: url(/img/logo-stacked-compact-tagline.svg)"></div> Alternatief: gestapeld compact</div>
</div>

<p class="mb-40">
<a class="inline-flex items-center text-lg font-medium no-underline text-white bg-blue-600 hover:bg-blue-700 rounded-md px-6 py-2 transition-colors" href="/uploads/digilab-brandbook.zip">
<svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 -ml-1 mr-2.5" viewBox="0 0 24 24"><path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 17v2a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-2M7 11l5 5l5-5m-5-7v12"/></svg>
Download logopack</a>
</p>


## Gebruik

Zorg voor voldoende ruimte rondom het logo. Voorkom dat het logo te dicht tegen een rand komt en plaats tekst niet te dicht bij het logo.

Zorg er dus voor dat er geen andere elementen zoals teksten of foto’s in de ruimtes rondom geplaatst worden.

<div class="mb-40"></div>


# Kleuren

## Overzicht

<div class="grid grid-cols-5 sm:grid-cols-9 gap-4 -mt-4 mb-40">
<div class="col-span-2 text-lg mt-8">
Primaire kleur:<br>
Donkerblauw
</div>

<div class="h-20 bg-[#083ce5] mt-8"></div>

<div class="col-span-2 font-semibold mt-8">DL donkerblauw</div>

<div>
<h4 class="mt-0">Hex</h4>
#083ce5
</div>

<div>
<h4 class="mt-0">RGB</h4>
R 8<br>
G 60<br>
B 229
</div>

<div class="col-span-2 sm:col-span-1">
<h4 class="mt-0">CMYK</h4>
C 97<br>
M 74<br>
Y 0<br>
K 10
</div>


<div class="col-span-2 text-lg mt-8">
Secundaire kleur:<br>
Lichtblauw
</div>

<div class="h-20 bg-[#00baff] mt-8"></div>

<div class="col-span-2 font-semibold mt-8">DL lichtblauw</div>

<div>
<h4 class="mt-0">Hex</h4>
#00baff
</div>

<div>
<h4 class="mt-0">RGB</h4>
R 0<br>
G 186<br>
B 255
</div>

<div class="col-span-2 sm:col-span-1">
<h4 class="mt-0">CMYK</h4>
C 100<br>
M 27<br>
Y 0<br>
K 0
</div>


<div class="col-span-2 text-lg mt-8">
Tekstkleur:<br>
Grijs
</div>

<div class="h-20 bg-[#212529] mt-8"></div>

<div class="col-span-2 font-semibold mt-8">DL tekst</div>

<div>
<h4 class="mt-0">Hex</h4>
#212529
</div>

<div>
<h4 class="mt-0">RGB</h4>
R 33<br>
G 37<br>
B 41
</div>

<div class="col-span-2 sm:col-span-1">
<h4 class="mt-0">CMYK</h4>
C 20<br>
M 10<br>
Y 0<br>
K 84
</div>
</div>


## Varianten

<div class="grid grid-cols-4 gap-4 text-center mb-8">
<div class="h-40 bg-contain bg-center bg-no-repeat" style="background-image: url(/img/logo-icon.svg)"></div>
<div class="h-40 bg-contain bg-center bg-no-repeat" style="background-image: url(/img/logo-icon-bicolor.svg)"></div>
<div class="h-40 bg-contain bg-center bg-no-repeat" style="background-image: url(/img/logo-icon-blue.svg)"></div>
<div class="h-40 bg-contain bg-center bg-no-repeat" style="background-image: url(/img/logo-icon-monochrome.svg)">
</div>

<div class="h-20 bg-contain bg-center bg-no-repeat" style="background-image: url(/img/logo-icon.svg)"></div>
<div class="h-20 bg-contain bg-center bg-no-repeat" style="background-image: url(/img/logo-icon-bicolor.svg)"></div>
<div class="h-20 bg-contain bg-center bg-no-repeat" style="background-image: url(/img/logo-icon-blue.svg)"></div>
<div class="h-20 bg-contain bg-center bg-no-repeat" style="background-image: url(/img/logo-icon-monochrome.svg)">
</div>

<div class="h-10 bg-contain bg-center bg-no-repeat" style="background-image: url(/img/logo-icon.svg)"></div>
<div class="h-10 bg-contain bg-center bg-no-repeat" style="background-image: url(/img/logo-icon-bicolor.svg)"></div>
<div class="h-10 bg-contain bg-center bg-no-repeat" style="background-image: url(/img/logo-icon-blue.svg)"></div>
<div class="h-10 bg-contain bg-center bg-no-repeat" style="background-image: url(/img/logo-icon-monochrome.svg)">
</div>
</div>

<p class="mb-40">Onderste rij: geen stippen in de 0’s.</p>


## Toepassingen

<div class="grid grid-cols-2 sm:grid-cols-3 gap-4 text-center mb-8">
<div><div class="h-44 bg-contain bg-center bg-no-repeat mb-2" style="background-image: url(/img/logo-basic.svg)"></div> Twee kleuren met verloop</div>
<div><div class="h-44 bg-contain bg-center bg-no-repeat mb-2" style="background-image: url(/img/logo-blue.svg)"></div> Eén kleur donkerblauw</div>
<div class="text-white bg-slate-900 p-4 rounded-md"><div class="h-44 bg-contain bg-center bg-no-repeat mb-2" style="background-image: url(/img/logo-white.svg)"></div> Diapositief</div>

<div><div class="h-44 bg-contain bg-center bg-no-repeat mb-2" style="background-image: url(/img/logo-bicolor.svg)"></div> Twee kleuren</div>
<div><div class="h-44 bg-contain bg-center bg-no-repeat mb-2" style="background-image: url(/img/logo-monochrome.svg)"></div> Zwart-wit</div>
<div class="text-white bg-slate-900 p-4 rounded-md"><div class="h-44 bg-contain bg-center bg-no-repeat mb-2" style="background-image: url(/img/logo-light-blue.svg)"></div> Lichtblauw</div>
</div>

<p class="mb-40">
<a class="inline-flex items-center text-lg font-medium no-underline text-white bg-blue-600 hover:bg-blue-700 rounded-md px-6 py-2 transition-colors" href="/uploads/digilab-brandbook.zip">
<svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 -ml-1 mr-2.5" viewBox="0 0 24 24"><path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 17v2a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-2M7 11l5 5l5-5m-5-7v12"/></svg>
Download logopack</a>
</p>


## Beeld

<div class="grid grid-cols-2 sm:grid-cols-4 gap-4 text-center mb-40">
<div>
<div class="h-60 bg-contain bg-center bg-no-repeat bg-slate-50 border-[1rem] border-slate-50 rounded-md mb-2" style="background-image: url(/img/logo-basic.svg)"></div> Logo met tekst
</div>
<div>
<div class="h-60 bg-contain bg-center bg-no-repeat bg-slate-900 border-[1rem] border-slate-900 rounded-md mb-2" style="background-image: url(/img/logo-basic.svg)"></div> Logo op donkere achtergrond
</div>
<div>
<div class="h-60 bg-contain bg-center bg-no-repeat bg-slate-500 border-[1rem] border-slate-500 rounded-md mb-2" style="background-image: url(/img/logo-outline.svg)"></div> Logo op grijze achtergrond
</div>
<div>
<div class="h-60 bg-slate-500 rounded-md mb-2" style="background-image: url(/img/logo-bg-pattern.svg); background-size: 3rem;">
<div class="bg-contain bg-center bg-no-repeat h-60 border-[1rem] border-transparent" style="background-image: url(/img/logo-outline.svg)"></div>
</div> Logo op achtergrondafbeeldingen
</div>
</div>


# Typografie

## Source Sans 3

Meer zakelijke letter, zonder té klinisch te worden. Strakke hoeken, maar niet geometrisch. De naam Source zegt het al, meer naar de digitale kant. Uitstekend bruikbaar op websites en in druk-uitingen. Variabel lettertype, met exact instelbare dikte. Opmerking: oudere versies van dit lettertype heetten *Source Sans Pro*.

<div class="h-96 bg-contain bg-left bg-no-repeat mb-10" style="background-image: url(/img/font-source-sans.svg)"></div>

<p class="mb-40">
<a class="inline-flex items-center align-[-0.25rem] mr-1.5 text-lg font-medium no-underline text-white bg-blue-600 hover:bg-blue-700 rounded-md px-6 py-2 transition-colors" href="https://fonts.google.com/specimen/Source+Sans+3" target="_blank">
<svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 -ml-1 mr-2.5" viewBox="0 0 24 24"><path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 17v2a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-2M7 11l5 5l5-5m-5-7v12"/></svg>
Download lettertype</a> van Google Fonts
</p>


## Source Code Pro

Kopregels, accenten en code. Lettertype om mee te spelen.

<div class="h-96 bg-contain bg-left bg-no-repeat mb-10" style="background-image: url(/img/font-source-code.svg)"></div>

<p class="mb-40">
<a class="inline-flex items-center align-[-0.25rem] mr-1.5 text-lg font-medium no-underline text-white bg-blue-600 hover:bg-blue-700 rounded-md px-6 py-2 transition-colors" href="https://fonts.google.com/specimen/Source+Code+Pro" target="_blank">
<svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 -ml-1 mr-2.5" viewBox="0 0 24 24"><path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 17v2a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-2M7 11l5 5l5-5m-5-7v12"/></svg>
Download lettertype</a> van Google Fonts
</p>


## Gebruik

Gebruik deze fonts in communicatie-uitingen zoals print- en drukwerk en externe presentaties. Doe dat altijd in overleg met de communicatie-verantwoordelijken.

De fonts moeten eerst gedownload en geïnstalleerd worden.

Voor interne en werkmiddelen gebruik je de standaardfonts van de applicaties.

<div class="h-40"></div>


## Opties

<p class="mb-40"><img src="/img/brandbook-font-options.png" alt="Lettertype-opties"></p>


# Illustraties

## Vlakken

<div class="h-20 rounded-lg bg-gradient-to-r from-[#00baff] to-[#083ce5] mb-8"></div>

<div class="relative border-x-2 border-gray-800 py-1 text-gray-500 mb-28">
<div class="h-0.5 bg-gray-800"></div>

<div class="absolute -top-1 left-0">
<div class="w-5 h-5 bg-[#00baff] rounded-full mx-auto"></div>
100% lichtblauw
</div>

<div class="absolute -top-1 right-0">
<div class="w-5 h-5 bg-[#083ce5] rounded-full mx-auto"></div>
100% donkerblauw
</div>
</div>

<div class="grid grid-cols-2 sm:grid-cols-4 gap-8 text-gray-500 text-center mb-40">
<div>
<div class="h-40 rounded-2xl bg-gradient-to-r from-[#00baff] to-[#083ce5] mb-2"></div>
Afronding 16
</div>

<div>
<div class="h-40 rounded-xl bg-gradient-to-r from-[#00baff] to-[#083ce5] mb-2"></div>
Afronding 12
</div>

<div>
<div class="h-40 rounded-lg bg-gradient-to-r from-[#00baff] to-[#083ce5] mb-2"></div>
Afronding 8
</div>

<div>
<div class="h-40 rounded bg-gradient-to-r from-[#00baff] to-[#083ce5] mb-2"></div>
Afronding 4
</div>
</div>


## Tekeningen

Benut de laboratoriummetafoor optimaal met lijntekeningen van apparaten. Plaats ze bij voorkeur groot en aflopend in een verloopkader.

<p class="mb-40"><img src="/img/brandbook-illustrations-drawings.png" alt="Tekeningen"></p>


## Binair

Benut de 'inside joke' van het beeldmerk met de binaire code. Is relatief makkelijk en snel om binaire getallen in 'loopstromen' op te nemen. Maakt het helemaal eigen.

<p class="mb-40"><img src="/img/brandbook-illustrations-binary.png" alt="Binair"></p>


# Assets

## Banners

<div class="grid sm:grid-cols-3 gap-4 mb-40">
<div class="h-96 bg-contain bg-center bg-no-repeat" style="background-image: url(/img/brandbook-banner1.png)"></div>
<div class="h-96 bg-contain bg-center bg-no-repeat" style="background-image: url(/img/brandbook-banner2.png)"></div>
<div class="h-96 bg-contain bg-center bg-no-repeat" style="background-image: url(/img/brandbook-banner3.png)"></div>
</div>


## Deurstickers

<p class="mb-40"><img src="/img/brandbook-assets-stickers.png" alt="Deurstickers"></p>


## Flyers

<p class="mb-40"><img src="/img/brandbook-assets-flyers.png" alt="Flyers"></p>


## Presentaties

<p class="mb-10"><img src="/img/brandbook-assets-presentations.png" alt="Presentaties"></p>

<p>
<a class="inline-flex items-center text-lg font-medium no-underline text-white bg-blue-600 hover:bg-blue-700 rounded-md px-6 py-2 transition-colors" href="/uploads/digilab-template-2024n.potx">
<svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 -ml-1 mr-2.5" viewBox="0 0 24 24"><path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 17v2a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-2M7 11l5 5l5-5m-5-7v12"/></svg>
Download presentatie-template</a>
</p>
