---
title : "Digilab — Innovatiewerkplaats voor het Federatief Datastelsel"
description: "Digilab is een broedplaats voor technische innovaties, bedoeld voor projecten die voor of met de overheid werken. Samen met deze projecten werkt Digilab aan het versterken van de technische interoperabiliteit en het verantwoord delen van data."
lead: "Innovatiewerkplaats voor het Federatief Datastelsel"
date: 2023-05-23T08:47:36+00:00
aliases:
  - /keywords/
---
