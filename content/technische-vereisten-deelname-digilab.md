---
title: "Technische vereisten deelname Digilab"
description: ""
date: 2023-11-02T17:05:34+02:00
sitemapExclude: true
lastmod: "2023-11-03"
---

Voor deelname aan het Digilab platform gelden de onderstaande technische vereisten.

De applicaties worden gedeployed in de Microsoft Azure cloud. Digilab probeert echter zo cloud-agnostisch mogelijk te zijn, waardoor het voor een applicatie niet zou moeten uitmaken waar deze precies draait.

Het platform van Digilab draait op Haven-compliant Kubernetes clusters. Dit stelt de volgende eisen aan de workloads:

- Elke workload moet gecontaineriseerd zijn, d.w.z. kunnen draaien in een (Docker) container.
- Elke container heeft een enkele functie, er mogen niet meerdere services draaien in één container.
- De workloads moeten stateless zijn, dus deze moeten niet schrijven naar hun local filesystem. Er kan wel geschreven worden naar databases en object storage.
- De container images dienen in een publiek beschikbaar registry te staan. Dit mag wel afgeschermd zijn met authenticatie. Het Digilab team kan hierin eventueel helpen.
- Gebruikte poorten moeten hoger zijn dan `1024`. Voor open source software kun je meestal de standaardpoort gebruiken, zoals `5432` voor PostgrSQL of `6379` voor Redis. Voor webapplicaties gebruik je bijv. `8000` of `8080`, maar hier ben je vrij in. De bijbehorende service heeft in het eerste geval dezelfde poort, maar in het tweede geval (webapplicatie) altijd poort `80`.
- Elke workload beschikt over een [livenessProbe en een readinessProbe](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/). Voor een webapplicatie moeten dit een lichtgewicht call zijn, bijvoorbeeld een simpele pagina die enkel een correcte statuscode teruggeeft.
- Voor elke workload zijn [resource requests en limits](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/) geconfigureerd, voor zowel `cpu` als `memory`.
- Elke applicatie moet compatible zijn met [Kubernetes Event-driven Autoscaling (KEDA)](https://keda.sh/) zodat er niet onnodig veel resources in gebruik blijven. Er is een [voorbeeldapplicatie](https://gitlab.com/digilab.overheid.nl/platform/flux/tenant-digilab-demo/-/tree/main/apps/autoscaling-demo) beschikbaar ter referentie.

<small>*Laatste wijziging: {{% lastmod-date %}}.*</small>
