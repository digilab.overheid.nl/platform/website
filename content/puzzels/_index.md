---
title: De puzzels van Digilab
draft: false
date: 2023-11-22T08:35:34+02:00
description: Puzzels op Digilab
---

Projecten op Digilab dragen allemaal bij aan het oplossen van één of meerdere puzzels op het gebied van technische interoperabiliteit en het slim en veilig delen van data. Ze zijn hieronder ingedeeld naar het <a href="https://federatief.datastelsel.nl/kennisbank/raamwerk/" target="_blank">raamwerk van federatief.datastelsel.nl</a> (gebaseerd op OpenDEI).

Mis je nog een puzzel? [Laat het ons weten!](https://digilab.overheid.nl/contact/)
