---
title: "Contact"
description: "Ben je geïnteresseerd in een samenwerking met Digilab, of heb je informatie van ons nodig?"
date: 2023-05-23T10:35:34+02:00
---

## Neem contact op

Ben je geïnteresseerd in een samenwerking met Digilab, of heb je informatie van ons nodig? Stuur ons een bericht:

<iframe class="-mx-4 max-w-3xl" src="https://contact-bot.core.digilab.network/" width="100%" height="220" style="margin: 0 -16px;" aria-label="Contact"></iframe>


## Doe mee

Sluit je aan bij de community:

<a class="inline-block text-lg no-underline font-medium text-white bg-blue-600 hover:bg-blue-700 rounded-md px-6 py-2 transition-colors" href="https://digilab.overheid.nl/chat">Ga naar onze online community →</a>

Je kunt inloggen met een Pleio-account. Heb je deze nog niet? Registreer je dan op de [website van Pleio](https://account.pleio.nl/register/).

Zie ook [de handleiding voor het eerste inlogmoment](/uploads/handleiding-eerste-inlog-mattermost.pdf).

![Onze online community (Mattermost)](/img/mattermost.png)
