---
title: Data bij de bron
draft: false
---
‘Data bij de bron’ houdt in dat gegevens van burgers en bedrijven alleen in bronsystemen zoals [de basisregistraties](https://www.digitaleoverheid.nl/overzicht-van-alle-onderwerpen/stelsel-van-basisregistraties/) worden geregistreerd, en niet gekopieerd of verstuurd. Zo zorgen we ervoor dat de overheid altijd transparante en actuele gegevens gebruikt.

Kijk voor meer informatie op [deze dossierpagina Data bij de Bron op de website Digitale Overheid](https://www.digitaleoverheid.nl/data-bij-de-bron).
