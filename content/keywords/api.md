---
title: API
draft: false
---
Een *application programming interface* wordt meestal aangeduid met de afkorting 'API'. Het is een set van regels en protocollen die er samen voor zorgen dat applicaties met elkaar kunnen communiceren.
