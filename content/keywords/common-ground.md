---
title: "Common Ground"
date: 2024-09-05T07:35:34+02:00
layout: keywords
---

Common Ground is de informatiekundige visie van gemeenten die richting geeft in de meerjarige transitie naar een moderne informatievoorziening.

Zie [commonground.nl](https://commonground.nl/) voor meer informatie.
