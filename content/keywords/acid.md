---
title: ACID
draft: false
---
ACID staat voor Atoomheid, Consistentie, Isolatie, en Duurzaamheid. Het is een belangrijke benadering voor het beheer van transacties in datasystemen waarbij data-integriteit en nauwkeurigheid van belang is. De (snelle) tegenhanger van deze benadering heet {{% keyword "BASE" %}}.
