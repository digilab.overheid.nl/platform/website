---
title: Event sourcing
draft: false
---
Event sourcing is een architectuurprincipe waarbij wijzigingen in de waarde van een gegeven altijd worden vastgelegd als een reeks onwijzigbare gebeurtenissen. Zie ook: [Digilab-project Event Sourcing](https://digilab.overheid.nl/projecten/event-sourcing/)
