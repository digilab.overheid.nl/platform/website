---
title: BASE
draft: false
---
BASE staat voor Basically Available, Soft state, Eventual consistency. Het is een benadering voor het beheer van transacties in datasystemen waarbij de snelle beschikbaarheid van gegevens prioriteit heeft. De (nauwkeurige) tegenhanger van deze benadering heet {{% keyword "ACID" %}}.
