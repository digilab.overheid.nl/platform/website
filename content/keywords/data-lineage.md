---
title: Data lineage
draft: false
---
Data lineage richt zich op de ontwikkeling van data vanaf het moment van onstaan tot nu: hoe worden gegevens gewijzigd, verrijkt of gecombineerd? Dit geeft een volledig beeld van de context en invloeden over de gehele levenscyclus van data.
