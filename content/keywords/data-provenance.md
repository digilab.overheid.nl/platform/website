---
title: Data provenance
draft: false
---
Het concept data provenance richt zich op de oorsprong of herkomst van gegevens: de bron, de auteur en de context waarin deze zijn ontstaan.
