---
title: "IMX: modelgedreven orkestratie"
draft: false
date: 2023-10-26T09:12:44.029Z
project-status: afgerond
thumbnail: /uploads/imx.png
description: Bij het raadplegen van meerdere bronnen heen combineert IMX als
  'orkestratielaag' gegevens tot het gewenste informatieproduct in de 'taal' van
  de gebruiker.
tags:
  - Interoperabiliteit > Dataservices
  - Vertrouwen > Toegang
  - Datawaarde > Publicatie
---
**Iedere gebruikerstoepassing heeft een specifieke informatiebehoefte, die in veel gevallen meerdere databronnen overstijgt. Toepassingen moeten daarom in staat zijn om op een betrouwbare manier gegevens van meerdere bronnen (via een {{% keyword "API" %}}) te raadplegen en waar nodig met elkaar te combineren tot het gewenste informatieproduct in de ‘taal’ van de gebruiker. Dit noemen we orkestratie.**

Bij het orkestreren van gegevens ontstaan diverse uitdagingen, zoals:

* Hoe kan door middel van open standaarden worden beschreven hoe een informatieproduct is samengesteld uit onderliggende bronnen, vanuit de juiste semantische samenhang?
* Hoe kunnen open én gesloten gegevens op een veilige manier worden gecombineerd? Hoe past dit in een federatief datastelsel?
* Op welke manier kan de herkomst van ieder individueel gegevenselement worden bijgehouden en beschikbaar worden gesteld?
* Op welke manier kan een acceptabele performance en hoge beschikbaarheid worden gerealiseerd, wat noodzakelijk is voor bedrijfskritische toepassingen?
* Wat voor eisen stelt dit aan de API’s van onderliggende bronnen? 

Het afgelopen jaar hebben Kadaster en Geonovum samengewerkt aan een model-gedreven oplossing, wat heeft geresulteerd in het IMX-concept. Dit concept bestaat uit een set open standaarden in combinatie met een slimme orkestratie-engine, die als open source beschikbaar wordt gesteld.

## Hoe ziet de samenwerking eruit?

Met diverse overheidsorganisaties wil Digilab gezamenlijk nieuwe use cases gaan beproeven, om te toetsen in hoeverre het IMX-concept hier invulling aan kan geven. Zo kunnen we ontdekken wat er al goed werkt, maar ook wat er nog ontbreekt of beter kan.

Meer informatie over de toepassing van het concept in geo-omgevingen vind je op [de website van Geonovum](https://www.geonovum.nl/geo-standaarden/imx-geo-semantisch-model-basis-en-kernregistraties).
