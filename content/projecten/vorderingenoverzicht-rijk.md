---
title: Vorderingenoverzicht Rijk
date: 2023-06-27T06:52:58.628Z
thumbnail: /uploads/vorderingenoverzicht-rijk.png
description: Dit project heeft als missie om burgers in staat te stellen
  gemakkelijk een overzicht van hun financiële verplichtingen aan
  overheidsorganisaties te verkrijgen.
project-status: lopend
tags:
  - "Interoperabiliteit > Dataservices"
  - "Vertrouwen > Identiteit"
  - "Vertrouwen > Toegang"
  - "Datawaarde > Publicatie"
  - "Datawaarde > Verantwoording"
  - "Governance > Beheer"
---
Het verzamelen van gegevens over actuele betalingsverplichtingen is voor burgers die in schuldhulpverlening terecht komen een grote drempel. Het project Vorderingenoverzicht Rijk levert een bijdrage aan het verlagen van die drempel, door burgers in staat te stellen snel en makkelijk een betrouwbaar overzicht van betalingsverplichtingen aan overheidsorganisaties in te zien. Op deze manier kunnen problemen met betalingen en schulden sneller worden opgelost of zelfs voorkomen. Lees meer op [de website van VO Rijk](https://www.vorijk.nl/docs/introductie/).

### Hoe ziet de samenwerking eruit?

VO Rijk ontwikkelt onder andere een referentie-applicatie die laat zien hoe het vorderingenoverzicht kan werken. De kern is echter een set standaarden, die het leveren van een dergelijk overzicht als dienst aan de burger mogelijk maakt. Dit is voor Digilab en andere projecten interessant omdat deze standaarden mogelijk herbruikbaar zijn, bijvoorbeeld in andere situaties waarin informatie over organisaties heen aan burgers of bedrijven getoond moeten worden.

De werkwijze van VO Rijk levert ook input aan belangrijke vraagstukken voor het Digilab en het Federatief Datastelsel zoals: 

* API standaardisering
* Historie/tijdreizen
* Vindbaarheid (semantisch) / Catalogus
* Gegevensbescherming
* Event sourcing
