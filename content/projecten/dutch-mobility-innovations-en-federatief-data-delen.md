---
title: "Dutch Mobility Innovations en federatief data delen"
draft: false
date: 2024-04-05T11:18:00+02:00
description: "Geonovum werkt in opdracht van DMI aan een digitale tweeling van
  de ruimtelijke infrastructuur, die ook data moet kunnen uitwisselen met andere
  omgevingen die zich niet in het geo-domein bevinden."
project-status: lopend
tags:
  - "Vertrouwen > Veiligheid"
  - "Datawaarde > Publicatie"
---
Op initiatief van het Ministerie van Infrastructuur en Waterstaat dragen verschillende organisaties in het platform Dutch Mobility Innovations (DMI) samen bij aan slimme en duurzame verstedelijking en mobiliteit. Stichting Geonovum werkt in opdracht van DMI aan een digitale tweeling van de ruimtelijke infrastructuur, die ook data moet kunnen uitwisselen met andere omgevingen die zich niet in het geo-domein bevinden. Voor de realisatie van deze interoperabiliteit werkt Geonovum samen met Digilab aan de hand van een aantal specifieke use-cases.

## Hoe ziet de samenwerking eruit
Het project richt zich in eerste instantie op het opzetten van een gefedereerde catalogus voor gebruik binnen gemeentes en provincies die zich leent voor een breder doel dan alleen het geospatiale domein, en voldoet aan moderne standaarden. Tegelijkertijd doen de teams van Digilab en het Federatief Datastelsel (FDS) juist kennis op over dit gespecialiseerde terrein.
