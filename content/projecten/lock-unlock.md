---
title: Lock - unlock
draft: false
date: 2023-12-11T08:43:30.422Z
description: Lock de data, unlock het potentieel! Binnen dit project onderzoeken
  we de mogelijkheden van autorisatie binnen Linked Data.
project-status: afgerond
tags:
  - "Vertrouwen > Identiteit"
  - "Vertrouwen > Toegang"
  - "Vertrouwen > Veiligheid"
  - "Datawaarde > Publicatie"
---
Het Lock – Unlock project wordt uitgevoerd in opdracht van het Federatief Datastelsel (FDS). Het kunnen delen van data is de kern van FDS, maar dat moet ook op een verantwoorde manier gebeuren waarbij de afscherming van data en autorisatie belangrijk zijn. Lock – Unlock focust zich vooral op Linked Data en bouwt verder op de [Integrale Gebruiksoplossing (IGO)](https://labs.kadaster.nl/cases/integralegebruiksoplossing) en de door Kadaster verder ontwikkelde [Kadaster Knowledge Graph (KKG)](https://labs.kadaster.nl/thema/Knowledge_graph). Voor autorisatie is in de wereld van Linked Data  nog weinig gestandaardiseerd beschikbaar. Daarom voeren we dit project uit om te onderzoeken en te beproeven wat hierin de (on)mogelijkheden zijn.

### Hoe ziet de samenwerking eruit?

Voor het project is het belangrijk om te laten zien dat verschillende datasets in samenhang én op een federatieve manier kunnen worden bevraagd. Daarom wordt een deel van de testdata op de omgeving van Digilab ontsloten met een SPARQL endpoint. Deze wordt in de demonstrator(s) gecombineerd met (open en gesloten) data van het Kadaster vanuit de Kadaster Knowledge Graph.

Verder delen we de opgedane kennis rondom autorisatie binnen Linked Data met Digilab zodat andere projecten hierop voort kunnen bouwen.

Meer weten over het project? Lees [dit artikel op de Pleio-omgeving van het programma Realisatie Interbestuurlijke Datastrategie (IBDS)](https://realisatieibds.pleio.nl/news/view/04652138-5853-4f9c-9700-87945211c5d5/lock-unlock-lock-de-data-unlock-het-potentieel).
