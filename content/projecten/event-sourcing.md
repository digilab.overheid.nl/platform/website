---
title: Event sourcing
draft: false
date: 2023-11-03T15:04:33.796Z
description: Event sourcing is een concept waarbij niet de gegevens centraal
  staan, maar de gebeurtenissen die ertoe geleid hebben.
project-status: lopend
tags:
  - "Interoperabiliteit > Modellen"
  - "Interoperabiliteit > Dataservices"
  - "Datawaarde > Verantwoording"
  - "Datawaarde > Publicatie"
---
**In een traditionele database worden gegevens opgeslagen als een statische momentopname van de werkelijkheid. Bij een aanpassing van de gegevens worden de oude gegevens overschreven, waarbij – zonder verdere ingrepen – niet duidelijk is wat de oude waarde was, en wat de aanleiding voor de aanpassing ervan. Event sourcing is een concept waarbij niet de gegevens centraal staan, maar de gebeurtenissen (events) die ertoe geleid hebben.**

Een systeem dat volgens de principes van *event sourcing* is opgebouwd is sneller en makkelijker in staat om in te spelen op veranderingen in wetgeving, of ingewikkelde aanpassingen met terugwerkende kracht. Daarom besteedt het Digilab-team tijd aan research en beproevingen op dit gebied, als een intern project.

De research die het Digilab-team besteedt aan dit onderwerp komt ook van pas bij diverse andere projecten, zoals [Synthetische Datageneratie](/projecten/synthetische-datageneratie/) en [Vorderingenoverzicht Rijk](/projecten/vorderingenoverzicht-rijk/).

## Meer weten?

Meld je aan voor de Digilab-community en volg [dit kanaal over event sourcing](https://digilab.overheid.nl/chat/digilab/channels/event-sourcing).
