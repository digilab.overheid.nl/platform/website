---
title: Logboek Dataverwerkingen
draft: false
date: 2023-10-10T14:04:00.243Z
project-status: lopend
thumbnail: /uploads/logboek-dataverwerkingen.png
description: De overheid wil voor burgers en bedrijven zo transparant mogelijk
  zijn in de omgang met hun gegevens. Daarom is het belangrijk om voor elke
  mutatie of raadpleging vast te leggen wie deze actie wanneer uitvoert, en
  waarom.
tags:
  - Vertrouwen > Veiligheid
  - Datawaarde > Verantwoording
aliases:
  - /projecten/verwerkingenlogging/
---
**De overheid wil voor burgers en bedrijven zo transparant mogelijk zijn in de omgang met hun gegevens. Daarom is het bij de informatieverwerking in datasets belangrijk om voor elke mutatie of raadpleging vast te leggen wie deze actie wanneer uitvoert, en waarom. Deze herleidbaarheid speelt zowel een rol in het kader van de wetgeving op het gebied van privacy als ook het streven naar openheid en transparantie bij de overheid. Voor een optimale samenwerking over organisaties en bronnen heen is voor deze logging een algemene standaard nodig.**

Het project *Logboek Dataverwerkingen* maakt deel uit van het [actieplan Data bij de Bron](https://www.digitaleoverheid.nl/data-bij-de-bron/) en onderzoekt met Digilab in samenwerking met diverse overheidspartijen (ministeries, uitvoeringsorganisaties en gemeentes) of we op basis van de tot nu toe opgedane inzichten een overheidsbrede standaard kunnen vaststellen. Na het succesvol beproeven van de standaard wordt deze voorgesteld voor opname in de [‘Pas toe of leg uit’-lijst](https://forumstandaardisatie.nl/pas-toe-leg-uit-beleid) van het Forum voor Standaardisatie.\
\
Ben je geïnteresseerd om bij te dragen? Neem een kijkje in de GitHub-repository van Logius en deel je inzichten via issues of stuur ons een bericht: https://github.com/Logius-standaarden/logboek-dataverwerkingen
