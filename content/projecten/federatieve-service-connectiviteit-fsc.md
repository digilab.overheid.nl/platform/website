---
title: Federatieve Service Connectiviteit (FSC)
draft: false
date: 2024-04-04T14:33:00.000Z
description: Deze standaard beschrijft een API-gateway die gebruik maakt van
  gefedereerde authenticatie, waarbij verbindingen tussen partijen worden
  beveiligd door middel van een wederzijds digitaal contract.
project-status: lopend
tags:
  - "Vertrouwen > Identiteit"
  - "Vertrouwen > Toegang"
  - "Vertrouwen > Veiligheid"
  - "Governance > Bestuurlijk"
---
FSC is een standaard voor het veilig geautomatiseerd uitwisselen van gegevens, die ontwikkeld is door de Vereniging van Nederlandse Gemeenten (VNG). De standaard beschrijft een API-gateway die gebruik maakt van gefedereerde authenticatie, waarbij verbindingen tussen partijen worden beveiligd door middel van een wederzijds digitaal contract.

## Hoe ziet de samenwerking eruit?

Digilab past de FSC-standaard toe in alle projecten. Daarnaast werkt het VNG-programma {{% keyword "Common Ground" %}} eraan om FSC vanuit het gemeentelijke niveau ‘op te tillen’ naar een landelijke standaard, door opname in de standaard Digikoppeling voor API’s.
Meer informatie vind je op [de website van FSC](https://nlx.io).

## Contact

Dit project heeft [een eigen kanaal in onze online chat-omgeving Mattermost](https://digilab.overheid.nl/chat/digilab/channels/fsc). Hier kun je direct contact opnemen met de deelnemers.
