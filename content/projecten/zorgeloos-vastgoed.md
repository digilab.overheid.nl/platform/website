---
title: Proeftuin Regie op Gegevens met Zorgeloos Vastgoed
draft: false
date: 2023-05-24T08:24:29.687Z
project-status: lopend
thumbnail: /uploads/zorgeloos-vastgoed.png
description: Bij de aan- en verkoop van een huis heb je in elke fase veel
  gegevens nodig van verschillende organisaties. Zorgeloos Vastgoed werkt aan
  een nieuw afsprakenstelsel dat gemak, duidelijkheid en zekerheid biedt aan
  alle partijen.
tags:
  - Vertrouwen > Toegang
  - Vertrouwen > Veiligheid
  - Datawaarde > Verantwoording
aliases:
  - /projects/zorgeloos-vastgoed
---
**Bij het kopen van een woning heb je als burger te maken met verschillende private dienstverleners die gegevens van je nodig hebben, zoals hypotheekverstrekkers en notarissen. In dit project wordt een oplossing beproefd die je als koper in staat stelt om jouw gegevens digitaal beschikbaar te stellen aan deze partijen, waarmee het proces van de aankoop makkelijker en sneller verloopt.**

Het beproeven van een veilige en transparante oplossing voor het ontsluiten van persoonsgegevens vanuit verschillende overheidsregistraties draagt bij aan de totstandkoming van het Federatief Datastelsel en de doelstellingen van [het programma Regie op Gegevens](https://www.digitaleoverheid.nl/overzicht-van-alle-onderwerpen/regie-op-gegevens/). Hierbij wordt niet alleen gekeken naar de techniek van de gegevensuitwisseling, maar ook naar organisatorische en juridische aspecten.
Meer achtergrondinformatie over het project vind je op de [de community-website van Regie op Gegevens](https://rog.pleio.nl).

Inmiddels is dit project afgerond en kan je het rapport vinden op de [Regie op Gegevens website](https://rog.pleio.nl/attachment/entity/56db3a5a-06f5-409a-9bee-09c494f16ceb).
