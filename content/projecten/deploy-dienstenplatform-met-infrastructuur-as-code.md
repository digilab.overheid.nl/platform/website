---
title: Deploy dienstenplatform met infrastructuur-as-code
draft: true
date: 2024-08-01T12:47:00+02:00
project-status: lopend
description: "De gemeente Utrecht heeft Infrastructuur-als-Code ingericht en wil
  dit graag samen met andere gemeenten beproeven door het opbouwen van het
  dienstenplatform, op een open en herbruikbare wijze. Dit dienstenplatform
  bevat een reeks opensource en herbruikbare componenten voor de burgerportal,
  low code workflow en bij behorende registraties. "
tags:
  - Interoperabiliteit > Modellen
  - Interoperabiliteit > Traceerbaarheid
  - Governance > Beheer
  - Vertrouwen > Veiligheid
---
De gemeente Utrecht heeft Infrastructuur-als-Code ingericht en wil dit graag samen met andere gemeenten beproeven door het opbouwen van het dienstenplatform, op een open en herbruikbare wijze. Dit dienstenplatform bevat een reeks opensource en herbruikbare componenten voor de burgerportal, low code workflow en bij behorende registraties.

Het dienstenplatform is in gebruik bij een aantal koplopergemeenten die het platform allemaal op een eigen wijze deployen. Dit leidt tot veel vertraging, handmatige handelingen en onnodig langzaam deployen. Doel van de proef is het toetsen en gezamenlijk verbeteren van code voor het inrichten van een landingszone voor orkestratie en het opbouwen (deployen) van het dienstenplatform. Dit moet leiden tot verbetering en breed gebruik bij gemeenten en wellicht landelijke overheden van de code.

Daarnaast levert de beproeving potentieel een laboratoriumomgeving op voor het testen van nieuwe onderdelen in het dienstenplatform door betrokken gemeenten en leveranciers.

### Hoe ziet de samenwerking eruit?
