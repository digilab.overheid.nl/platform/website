---
title: ACME-protocol
draft: false
date: 2023-10-09T14:07:10.220Z
thumbnail: /uploads/acme-protocol.png
description: "De ACME-omgeving bevat een protocol voor het geautomatiseerd
  uitgeven en vernieuwen van beveiligingscertificaten. Dit zou het beheer van
  deze certificaten bij de overheid efficiënter en minder foutgevoelig maken."
project-status: lopend
tags:
  - "Vertrouwen > Toegang"
  - "Vertrouwen > Veiligheid"
  - "Datawaarde > Publicatie"
aliases:
  - /projecten/beproeving-acme-protocol/
---
**Beveiligingscertificaten zijn unieke sleutels waarmee digitale systemen zich identificeren bij het online verbinden met een ander systeem. Bij de communicatie tussen overheidssystemen worden hiervoor speciale ‘PKI Overheid-certificaten’ gebruikt. Deze certificaten hebben een vaste geldigheidsduur, waardoor het beheer de nodige risico’s met zich meebrengt. Zo leidt het niet tijdig vervangen van certificaten met enige regelmaat tot grote storingen in belangrijke digitale systemen. De ‘Automatic Certificate Management Environment’ (ACME) bevat een protocol voor het geautomatiseerd uitgeven en vernieuwen van deze certificaten. De toepassing ervan zou het beheer van deze certificaten efficiënter en beduidend minder foutgevoelig maken.**

## Flexibiliteit en schaalbaarheid

Met de komst van een Federatief Datastelsel zal het aantal koppelingen tussen overheidssystemen alleen maar toenemen, en daarmee ook het aantal gebruikte certificaten. De toepassing van het ACME-protocol zorgt ervoor dat zo’n toename niet leidt tot een verzwaring van de beheerlast, waardoor het proces van uitgeven en vernieuwen van deze certificaten ongehinderd blijft. 

## Hoe ziet de samenwerking eruit?

Het actieplan Data bij de Bron richt zich op de bevordering van technische interoperabiliteit bij overheidssystemen, om zo het ontsluiten van databronnen optimaal te faciliteren. De implementatie van het ACME-protocol draagt bij aan dit doel, en daarom is deze activiteit als project opgenomen in het actieplan. De toepassing van dit protocol wordt beproefd in Digilab, en bij een succesvol resultaat is het de bedoeling dat Logius gaat zorgen dat het ACME-protocol ondersteund wordt door alle relevante leveranciers.
