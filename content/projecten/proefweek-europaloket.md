---
layout: project
title: Proefweek Europaloket
draft: false
date: 2024-05-03T16:47:00+02:00
description: In navolging van het succesvolle fieldlab Innovatieve
  Gegevensuitwisseling eerder dit jaar organiseren Digilab en de DG Migratie van
  het ministerie van Justitie en Veiligheid een nieuwe meerdaagse digitale
  beproeving.
project-status: lopend
tags:
  - "Vertrouwen > Toegang"
  - "Vertrouwen > Veiligheid"
  - "Interoperabiliteit > Dataservices"
---
**In navolging van het succesvolle fieldlab Innovatieve Gegevensuitwisseling eerder dit jaar organiseren Digilab en de DG Migratie van het ministerie van Justitie en Veiligheid een nieuwe meerdaagse digitale beproeving. Deze keer met gegevensuitwisseling binnen de Europese Unie als onderwerp.**

In het kader van samenwerking binnen de EU werkt het ministerie aan het aansluiten van drie ketens op Europese systemen:

* Migratieketen
* Strafrechtketen
* Veiligheidsketen

"De samenwerking met Digilab is JenV goed bevallen.", licht projectleider Mike Dell toe. "Daarom willen we tijdens deze proefweek op een vergelijkbare manier onderzoeken hoe deze ketens samen de Beta-API kunnen gebruiken in combinatie met een een oplossing voor API-management. We hebben er alle vertrouwen in dat deze werkvorm zich daar prima voor leent." 

Lees meer over het fieldlab Innovatieve Gegevensuitwisseling:

* [aankondiging op de website van Digilab](https://digilab.overheid.nl/artikelen/fieldlab-samen-naar-een-nieuwe-architectuur/)
