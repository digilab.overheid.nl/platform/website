---
title: Federatieve toegangsverlening
draft: false
date: 2024-08-05T10:52:00+02:00
project-status: lopend
description: "Het project federatieve toegangsverlening (FTV) heeft als doel een
  standaard voor te stellen voor autorisatie in een federatief datastelsel. Hoe
  zorg je als aanbiedende en afnemende partijen samen dat de
  verantwoordelijkheden rondom toegang goed en eerlijk verdeeld zijn? Daar is
  nog geen standaard voor, waardoor er versnippering is ontstaan en er veel werk
  gaat zitten in beheer."
tags:
  - Vertrouwen > Veiligheid
aliases:
  - /projecten/toegangsverleningmethodiek-api/
---
Samenwerken in een federatief datastelsel biedt vele voordelen. Meer partijen kunnen aansluiten zonder dat het beheer door de aanbieders toeneemt, er kunnen meer gegevens gecombineerd worden voor data-analyse, en burgers kunnen meer zicht krijgen op hoe er met hun gegevens wordt omgegaan. Daarbij is grip op de toegang belangrijk: wie mag wat, en onder welke voorwaarden? 

Denk hierbij aan gemeenten die verantwoording moeten afleggen over de grondslagen en doelbinding van hun verwerkingen, inzicht in de regels die daarvoor gelden en makkelijker kunnen aanpassen van die regels.

Door een standaard voor te stellen die partijen kunnen inzetten in hun implementaties, inclusief de nodige ondersteuning in zowel technische als organisatorische zin, hoopt dit project een verantwoord FDS dichterbij te brengen.
 
## Meer weten?
Het project en de mensen zijn hier te vinden:

[Gitlab](https://federatieve-toegangsverlening-digilab-overheid-n-5d4b9badc9bcfa.gitlab.io/)

[Mattermost](https://digilab.overheid.nl/chat/digilab/channels/federatieve-toegangsverlening)

[Mail](mailto:marc.deboer@vng.nl)
