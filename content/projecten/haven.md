---
title: Haven
draft: false
date: 2023-09-12T20:16:21.613Z
thumbnail: /uploads/haven.png
description: "Haven is een standaard voor platform-onafhankelijke cloudhosting,
  ontstaan vanuit een sterke behoefte om applicaties te ontwikkelen die
  makkelijk herbruikbaar zijn voor alle Nederlandse gemeenten. "
project-status: lopend
tags:
  - "Interoperabiliteit > Dataservices"
  - "Vertrouwen > Veiligheid"
  - "Governance > Operationeel"
---
Haven is een standaard voor platform-onafhankelijke cloudhosting, ontstaan vanuit een sterke behoefte om applicaties te ontwikkelen die makkelijk herbruikbaar zijn voor alle Nederlandse gemeenten. De standaard maakt onderdeel uit van {{% keyword "Common Ground" %}} en is al in gebruik bij de Vereniging van Nederlandse Gemeenten (VNG).

### Hoe ziet de samenwerking eruit?

De technische infrastructuur van Digilab is Haven-compliant, en samen met andere deelnemende projecten onderzoeken we hoe deze standaard ook voor andere overheidsorganisaties gebruikt kan worden.

Meer informatie vind je op de website van Common Ground: <https://haven.commonground.nl/over-haven>
