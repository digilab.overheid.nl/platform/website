---
title: "Projecten op Digilab"
description: "Projecten en beproevingen op Digilab"
date: 2023-08-15T10:35:34+02:00
---

Digilab werkt aan verschillende projecten. Dat kunnen projecten zijn die een overheidsorganisatie in samenwerking met ons uitvoert, maar we hebben ook een goed gevulde backlog met eigen beproevingen.
