---
title: Synthetische datageneratie
draft: false
date: 2023-10-18T12:34:20.274Z
thumbnail: /uploads/synthetische-datageneratie.png
description: Een oplossing die op basis van het principe van event sourcing in
  een snel tempo een grote hoeveelheid representatieve testdata kan opleveren.
project-status: lopend
tags:
  - "Interoperabiliteit > Modellen"
  - "Interoperabiliteit > Dataservices"
  - "Datawaarde > Verantwoording"
  - "Datawaarde > Publicatie"
---
**Elk project dat beproevingen wil uitvoeren over meerdere databronnen heen heeft behoefte aan consistente testdata. Deze gegevens moeten representatief genoeg zijn om bruikbare resultaten mee te bereiken, zowel kwalitatief als kwantitatief.**

Op die twee vlakken doen zich vaak problemen voor: 

* **Kwalitatief** – inconsistentie tussen bronnen: als een testscript onderzoekt wat de consequenties zijn van een verhuizing voor de parkeervergunning, van persoon X, dan moeten zijn of haar gegevens zowel in de bron met persoonsgegevens als in die met vergunningsgegevens voorkomen.
* **Kwantitatief** - het samenstellen van een grote representatieve testset is een complexe operatie. Daarom werd het verleden vaak langere tijd achtereen met dezelfde gegevens gewerkt. Een statische testset groeit niet vanzelf mee met de steeds veranderende samenleving, en daarmee verliezen deze gegevens snel hun relevantie. 

Het project Synthetische Datageneratie levert een oplossing die op basis van het principe van *event sourcing* in een snel tempo een grote hoeveelheid representatieve testdata kan opleveren. Hierbij wordt extreem schalende cloudtechnologie toegepast, die in korte tijd een reeks gebeurtenissen (zoals geboorte, huwelijk, scheiding of overlijden) kan toepassen op een of meerdere gegevensbronnen. Het resultaat: een consistente gegevensverzameling die snel en makkelijk opnieuw gegenereerd kan worden.

### Hoe ziet de samenwerking eruit?

Dit is een 'eigen' project uit de backlog van Digilab. Heeft jouw project raakvlakken met dit onderwerp? Dan komen we graag met je in contact om te onderzoeken of we samen een beproeving kunnen doen! Stuur ons een bericht via [onze contactpagina](https://digilab.overheid.nl/contact/) of bekijk meteen [het kanaal over dit project](https://digilab.overheid.nl/chat/digilab/channels/synthetische-data) in onze digitale community.
