---
title: Uit betrouwbare bron
draft: false
date: 2024-07-23T16:06:00+02:00
project-status: lopend
description: In een federatief datastelsel gaan we ervan uit dat een groot
  aantal registers wordt ontsloten volgens het principe ‘data bij de bron’.  Om
  deze toepassing goed te laten slagen is de kwaliteit van de gegevens in deze
  bronnen van groot belang.
tags:
  - Interoperabiliteit > Modellen
  - Interoperabiliteit > Dataservices
  - Datawaarde > Verantwoording
---
In een federatief datastelsel gaan we ervan uit dat een groot aantal registers wordt ontsloten volgens het principe ‘data bij de bron’.  Voor een optimale dienstverlening is de kwaliteit van de gegevens in deze bronnen essentieel. Deze moeten in staat zijn om antwoord te geven op vragen over de historie van gegevens, of over de aanleiding van de registratie ervan. Dit project werkt aan de opzet van dergelijke ‘capabele’ bronnen, om ervoor te zorgen dat de overheid een deskundige en bekwame opdrachtgever kan zijn voor de bouw of verwerving van toekomstige registers.

### Hoe ziet de samenwerking eruit?
Samen met het team van Digilab bouwt het project ‘Uit betrouwbare bron’ allereerst een prototype van een ‘capabel’ register, gebaseerd op de beginselen van claim sourcing. Vervolgens wordt op basis van dit prototype als proof of concept een klein deel van een register gebouwd voor twee verschillende toepassingsdomeinen: het WOZ-domein en de registratie van wegennetwerkgegevens.

### Meer weten?
Op [de website van het project](http://www.uitbetrouwbarebron.nl) vind je meer uitgebreide documentatie.
